package com.zsn.springbootmybatis.controller;

import com.zsn.springbootmybatis.bean.Doubanc;
import com.zsn.springbootmybatis.mapper.DoubancMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName HelloController
 * @Description TODO
 * @Author lhx
 * @Date 2020/3/16 11:55
 * @Version 1.0
 **/
@RestController
public class HelloController {

    @Autowired
    DoubancMapper doubancMapper;

    @GetMapping("/test/{id}")
    public Doubanc getContents(@PathVariable("id") Integer id) {
        return  doubancMapper.getById(id);
    }
}
