package com.zsn.springbootmybatis.mapper;

import com.zsn.springbootmybatis.bean.Doubanc;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface DoubancMapper {

    @Select("select * from doubanc where id = #{id}")
    public Doubanc getById(Integer id);
}
