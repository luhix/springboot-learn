package com.zsn.springbootmybatis.bean;

/**
 * @ClassName Doubanc
 * @Description TODO
 * @Author lhx
 * @Date 2020/3/16 11:52
 * @Version 1.0
 **/
public class Doubanc {

    private Integer id;
    private String country;
    private Integer nums;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getNums() {
        return nums;
    }

    public void setNums(Integer nums) {
        this.nums = nums;
    }
}
