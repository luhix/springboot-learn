package com.zsn.springbootjpa.controller;

import com.zsn.springbootjpa.enetity.User;
import com.zsn.springbootjpa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName UserController
 * @Description TODO
 * @Author lhx
 * @Date 2020/3/17 14:45
 * @Version 1.0
 **/
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/count")
    public int countUsers() {
        return userService.getCount();
    }

    @GetMapping("/getUser")
    public List<User> getList() {
        List<User> users = userService.findFirst3ByName("ls");
        System.out.println(users);
        return users;
    }

}
