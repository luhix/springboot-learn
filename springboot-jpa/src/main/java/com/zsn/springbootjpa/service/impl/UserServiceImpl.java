package com.zsn.springbootjpa.service.impl;

import com.zsn.springbootjpa.domain.UserRepository;
import com.zsn.springbootjpa.enetity.User;
import com.zsn.springbootjpa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName UserServiceImpl
 * @Description TODO
 * @Author lhx
 * @Date 2020/3/17 14:41
 * @Version 1.0
 **/
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public int getCount() {
        return userRepository.countUser();
    }

    @Override
    public List<User> findFirst3ByName(String name) {
        return userRepository.findFirst3ByName(name);
    }



}
