package com.zsn.springbootjpa.enetity;

import javax.persistence.*;

/**
 * @ClassName User
 * @Description TODO
 * @Author lhx
 * @Date 2020/3/17 14:29
 * @Version 1.0
 **/
@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
