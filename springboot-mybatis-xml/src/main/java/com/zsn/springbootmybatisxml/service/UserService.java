package com.zsn.springbootmybatisxml.service;

import com.zsn.springbootmybatisxml.pojo.User;

import java.util.List;

public interface UserService {
    User findOne(Integer id);

    List<User> selectUser();

    int addUser(User user);
    int updateUser(User user);

    // 根据id删除用户
    int deleteUser(Integer id);
}
