package com.zsn.springbootmybatisxml.service.impl;

import com.zsn.springbootmybatisxml.mapper.UserMapper;
import com.zsn.springbootmybatisxml.pojo.User;
import com.zsn.springbootmybatisxml.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName UserServiceImpl
 * @Description TODO
 * @Author lhx
 * @Date 2020/3/17 10:25
 * @Version 1.0
 **/

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper ;

    @Override
    public User findOne(Integer id) {
        return userMapper.getOne(id);
    }

    @Override
    public List<User> selectUser() {
        return userMapper.selectUser();
    }

    @Override
    public int addUser(User user) {
        return userMapper.addUser(user);
    }

    @Override
    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }

    @Override
    public int deleteUser(Integer id) {
        return userMapper.deleteUser(id);
    }


}
