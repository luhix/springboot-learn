package com.zsn.springbootmybatisxml.controller;

import com.zsn.springbootmybatisxml.pojo.User;
import com.zsn.springbootmybatisxml.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName UserController
 * @Description TODO
 * @Author lhx
 * @Date 2020/3/17 10:26
 * @Version 1.0
 **/
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/getOne/{id}")
    public User getOne(@PathVariable("id") Integer id) {
        return (User) userService.findOne(id);
    }

    @GetMapping("/getAll")
    public List<User> getUser() {
        List<User> users = userService.selectUser();
        return users;
    }

    @GetMapping("/addUser")
    public int addUser() {
        User user = new User();
        user.setId(3);
        user.setName("王五");

        return userService.addUser(user);
    }

    @GetMapping("/updateUser")
    public int updateUser(User user) {
        user.setId(3);
        user.setName("sdsdww");
        return userService.updateUser(user);
    }
}
