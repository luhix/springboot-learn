package com.zsn.springbootmybatisxml.pojo;

/**
 * @ClassName User
 * @Description TODO
 * @Author lhx
 * @Date 2020/3/17 10:11
 * @Version 1.0
 **/
public class User {
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
