package com.zsn.springbootmybatisxml.mapper;

import com.zsn.springbootmybatisxml.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName UserMapper
 * @Description TODO
 * @Author lhx
 * @Date 2020/3/17 10:11
 * @Version 1.0
 **/
@Mapper
public interface UserMapper {
    // 查询单个用户
    User getOne(Integer id);

    // 查询所有用户
    List<User> selectUser();

    // 添加一个用户
    int addUser(User user);

    // 修改一个用户
    int updateUser(User user);

    // 根据id删除用户
    int deleteUser(Integer id);
}
