package com.zsn.controller;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.io.IOException;

/**
 * @ClassName JmsConsumerTopic
 * @Description TODO
 * @Author lhx
 * @Date 2020/4/3 16:00
 * @Version 1.0
 **/
public class JmsConsumerTopic {

    private static  final String ACTIVEMQ_URL="tcp://192.168.0.54:61616";
    private static final String TOPIC_NAME = "topic01";

    public static void main(String[] args) throws JMSException, IOException {

        // 1.创建连接工厂，按照给定的URL地址，采用默认的用户名和密码
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);

        // 2. 通过连接工厂，获得连接connection 并启动访问
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();

        // 3.创建会话，
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);


        // 4.创建目的地（具体是队列queue或主题topic）
        Topic topic = session.createTopic(TOPIC_NAME);


        // 5.创建消息的生成者
        MessageConsumer consumer = session.createConsumer(topic);

        consumer.setMessageListener(new MessageListener() {
            @SneakyThrows
            @Override
            public void onMessage(Message message) {
                TextMessage textMessage = (TextMessage) message;
                if (null != message && message instanceof TextMessage) {
                    System.out.println("消费者监听到的Topic消息******" + textMessage.getText());
                }
            }
        });

        // 手动阻塞进程
        System.in.read();
        consumer.close();
        session.close();
        connection.close();
    }
}
