package com.zsn.controller;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * @ClassName JmsProducerTopic
 * @Description TODO
 * @Author lhx
 * @Date 2020/4/3 15:48
 * @Version 1.0
 **/
public class JmsProducerTopic {

    private static  final String ACTIVEMQ_URL="tcp://192.168.0.54:61616";
    private static final String TOPIC_NAME = "topic01";

    public static void main(String[] args) throws JMSException {

        // 1.创建连接工厂，按照给定的URL地址，采用默认的用户名和密码
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);

        // 2. 通过连接工厂，获得连接connection 并启动访问
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();

        // 3.创建会话，
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // 4.创建目的地（具体是队列queue或主题topic）
        Topic topic = session.createTopic(TOPIC_NAME);

        // 5.创建消息的生成者
        MessageProducer producer = session.createProducer(topic);

        // 通过生成者发送消息
        for (int i = 0; i < 3; i++) {
            // 7.创建消息
            TextMessage textMessage = session.createTextMessage("topic-- " + i);

            // 8. 发送给MQ
            producer.send(textMessage);
        }

        // 8. 关闭资源
        producer.close();
        session.close();
        connection.close();

        System.out.println("******topic消息发送到MQ******");
    }

}
