package com.zsn.controller;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.io.IOException;

/**
 * @ClassName JmsConsumer
 * @Description TODO
 * @Author lhx
 * @Date 2020/4/3 15:10
 * @Version 1.0
 **/
public class JmsConsumer {

    public static final String ACTIVEMQ_URL = "tcp://192.168.0.54:61616";
    public static final String QUEUE_NAME = "queue01";

    public static void main(String[] args) throws JMSException, IOException {
        // 1. 创建连接工厂，按照给定的URL地址，采用默认用户名密码
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);

        // 2. 通过连接工厂，获得连接Connection 启动并访问
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();

        // 3. 创建会话session
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // 4. 创建目的地（具体队列queue或者主题topic）
        Queue queue = session.createQueue(QUEUE_NAME);

        // 5. 创建消费者
        MessageConsumer consumer = session.createConsumer(queue);
        /*while (true) {

            // 收到消息前一直阻塞进程
            TextMessage textMessage = (TextMessage) consumer.receive();
            if (textMessage != null) {
                System.out.println("******消费者收到消息" + textMessage.getText());
            } else {
                break;
            }
        }*/

        //


        //消费者监听器方式接收消息 监听器方式属于异步非阻塞方式，所以需要手动阻塞进程
        consumer.setMessageListener(new MessageListener() {

            @lombok.SneakyThrows
            @Override
            public void onMessage(Message message) {
                if (null != message && message instanceof TextMessage) {

                    TextMessage textMessage = (TextMessage) message;

                    System.out.println("******消费者收到消息" + textMessage.getText());
                }
            }
        });

        // 收到阻塞进程
        System.in.read();

        // 6. 关闭资源
        consumer.close();
        session.close();
        connection.close();
    }
}
