package com.zsn.controller;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * @ClassName JmsProducer
 * @Description TODO
 * @Author lhx
 * @Date 2020/4/3 14:56
 * @Version 1.0
 **/
public class JmsProducer {

    public static final String ACTIVEMQ_URL = "tcp://192.168.0.54:61616";
    public static String QUEUE_NAME = "queue01";

    public static void main(String[] args) throws JMSException {

        // 1. 创连接工厂, 按照给的的URL 地址， 采用默认用户名密码
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);

        // 2. 通过连接工厂，获得连接Connection 并启动
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();

        // 3. 创建会话
        // 两个参数 第一个是控制事务，第二个是签收控制
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // 4. 创建目的地（具体是队列queue 或者 topic）
        Queue queue = session.createQueue(QUEUE_NAME);

        // 5. 创建消息的生产者
        MessageProducer producer = session.createProducer(queue);

        // 6. 通过消息生成者发送消息
        for (int i = 0; i < 10; i++) {

            // 7. 创建消息
            TextMessage textMessage = session.createTextMessage("msg------" + i);

            // 8. 发送给mq
            producer.send(textMessage);
        }

        // 9. 关闭资源
        producer.close();
        session.close();
        connection.close();

        System.out.println("******MQ消息发送完成******");
    }

}
